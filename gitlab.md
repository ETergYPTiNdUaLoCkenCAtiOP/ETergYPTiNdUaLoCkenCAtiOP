## A list of useful link for new GitLab users to checkout
- [How to make my first Git commit?](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html)
- [How can I check project and group visibility?](https://gitlab.cecs.anu.edu.au/help/user/public_access.md)
- [I want to edit the `.md` file, where can I find supported syntax?](https://gitlab.cecs.anu.edu.au/help/user/markdown.md)
- [If I have a Git repository, how can I import it to GitLab?](https://gitlab.cecs.anu.edu.au/help/user/project/import/repo_by_url.md)
- [How can I transfer my project to another namespace?](https://gitlab.cecs.anu.edu.au/help/user/project/settings/index#transfer-a-project-to-another-namespace)
- [If I want to program GitLab operations, how can I do this?](https://gitlab.cecs.anu.edu.au/help/api/index.md#how-to-use-the-api)
- [Now I know how to program GitLab operations through GitLab API, but where are the documentation for all the operation that I can use?](https://gitlab.cecs.anu.edu.au/help/api/api_resources.md)
- [If I want to add any GitLab user to my project, how can I do this?](https://docs.gitlab.com/ee/user/project/members/#add-users-to-a-project)
- [How to protect my GitLab account better?](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#one-time-password)
- [Once I enabled 2FA, I found I can't use my password to clone the repository though HTTPS. How can I get private token?](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
