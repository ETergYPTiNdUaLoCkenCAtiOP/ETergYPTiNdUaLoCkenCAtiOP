# Hi, I am GitLab cat. I am always annoying, but I could be also useful.
## About me
I am an account for doing many admin jobs on Gitlab. You would see my account on quite a lot of projects you own. The account is used for - 
1. give permission to GitLab users.
2. help GitLab users to find some missing repositories.
## A few useful links you might want to know
1. [Student Linux Lab Computing Environment Documentation](https://cs.anu.edu.au/docs/student-computing-environment/)
2. [ANU Jekyll page Documentation](https://cs.anu.edu.au/docs/jekyll-anu-theme-docs/)
3. [Lucy - a Marking tool used on Gitlab](https://cs.anu.edu.au/docs/lucy/)
4. [Gitlab course setup - internal](https://itgroup.cecs.anu.edu.au/wiki/index.php/Teaching_GitLab_course_setup)
5. [Course Debian package](http://cecslabs-pkgs.cecs.anu.edu.au/)
6. [GitLab filehook repository - private](https://gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin)
7. [GitLab new user knowledge list](https://gitlab.cecs.anu.edu.au/ETergYPTiNdUaLoCkenCAtiOP/ETergYPTiNdUaLoCkenCAtiOP/-/blob/main/gitlab.md)
8. [GitLab docker registry](https://gitlab.cecs.anu.edu.au/ETergYPTiNdUaLoCkenCAtiOP/ETergYPTiNdUaLoCkenCAtiOP/-/blob/main/registry.md)
## Talk to me
[Service Desk](https://services.anu.edu.au/information-technology/software-systems/service-now/logging-an-it-service-desk-job)
