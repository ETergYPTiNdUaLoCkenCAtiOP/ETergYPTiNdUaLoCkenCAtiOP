## ANU GitLab Docker Registry
ANU GitLab Docker Registry is hosted on `prod-repo.cecs.anu.edu.au`. Just like `hub.docker.com`, and it requires authentication with Research GitLab to upload an docker image.

## Limitation
1. The registry only has 130G disk space at the moment. We would work out to add more disk space if it is required in the futher. If you need to upload a lot of docker images to the registry please let us know firstly.
2. As it is Mar 29, 2023, it doesn't have a backup for the registry. For the disaster recovery, please upload your `Dockerfile` to GitLab instances where we have backup for the Git repository.

## How to upload a docker image?
1. Login to `gitlab.anu.edu.au`.
2. Create a new project - for example, let's call it `https://gitlab.anu.edu.au/uxxxxx/helloworld`
3. Open `Container registry` at `Settings` -> `General` -> `Visibility, project features, permissions` -> `Container registry` -> `Save`
4. Go to `https://gitlab.anu.edu.au/uxxxxx/helloworld/container_registry` and follow the upload instructions.

Login
```
docker login prod-repo.cecs.anu.edu.au:443
```
Build an image
```
docker build -t prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld .
docker build -t prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld:v2.2.0 .
docker build -t prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld/another_image:latest .
```
Tag an image from another docker image
```
docker tag <another docker image> prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld
docker tag <another docker image> prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld/another_image
```
Upload docker image
```
docker push prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld
docker push prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld/another_image
```
## Can I upload docker image to a group?
The docker image has to be with the project, but you can create a project under the group and upload the image to the group project.

## How to make it public for non GitLab research users?
The GitLab docker image is with project. To make the image public, just change the visibility of the project to be public.

## How can it use for CI/CD?
In the first line of `.gitlab-ci.yml`, change it to the following.
`image: prod-repo.cecs.anu.edu.au:443/uxxxxx/helloworld:v2.2.0`
